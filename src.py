#!/usr/bin/env python3

from tkinter import *
from PIL import Image as PILImage, ImageTk as PILImageTk
from tkinter import filedialog, Canvas

minCanvasWidth, minCanvasHeight = 200, 200

def openImage():
	global pilImage, filename, tkImage
	filename = filedialog.askopenfilename(parent=root)
	pilImage = PILImage.open(filename)
	displayImage()

def saveImage():
	global pilImage
	pilImage.save(filename)

def saveAs():
	global filename
	filename = filedialog.asksaveasfilename(parent=root)
	pilImage.save(filename)

def displayImage():
	global canvas, pilImage, tkImage
	tkImage = PILImageTk.PhotoImage(pilImage)

	width = minCanvasWidth if minCanvasWidth > tkImage.width() else tkImage.width()
	height = minCanvasHeight if minCanvasHeight > tkImage.height() else tkImage.height()

	canvas.config(width=width, height=height, scrollregion=(0, 0, width, height))
	canvas.create_image(0, 0, anchor=NW, image=tkImage)

def transposeImage(imageData, dimensions, direction):
	width, height = dimensions
	newImage = []
	if direction == 'LEFT_RIGHT':
		for h in range(height):
			for w in range(width - 1, -1, -1):
				newImage.append(imageData[h * width + w])
	elif direction == 'TOP_BOTTOM':
		for h in range(height - 1, -1, -1):
			for w in range(width):
				newImage.append(imageData[h * width + w])

	return newImage

def flipHor():
	global pilImage, previousPilImage
	previousPilImage = pilImage
	imgData = transposeImage(list(pilImage.getdata()), pilImage.size, 'LEFT_RIGHT')
	pilImage = PILImage.new(pilImage.mode, pilImage.size)
	pilImage.putdata(imgData)
	displayImage()

def flipVer():
	global pilImage, previousPilImage
	previousPilImage = pilImage
	imgData = transposeImage(list(pilImage.getdata()), pilImage.size, 'TOP_BOTTOM')
	pilImage = PILImage.new(pilImage.mode, pilImage.size)
	pilImage.putdata(imgData)
	displayImage()

def desaturate():
	global pilImage, previousPilImage
	if pilImage.mode == 'P' or pilImage.mode == 'L':
		return
	previousPilImage = pilImage
	oldImage = pilImage.getdata()
	width, height = pilImage.size
	newImage = []
	for h in range(height):
		for w in range(width):
			r, g, b = oldImage[h * width + w]
			newImage.append(int(r * 0.3 + g * 0.59 + b * 0.11))

	# print(newImage)
	pilImage = PILImage.new('L', pilImage.size)
	pilImage.putdata(newImage)
	displayImage()

def invertColors():
	global pilImage, previousPilImage
	previousPilImage = pilImage
	oldImage = pilImage.getdata()
	width, height = pilImage.size
	newImage = []
	for h in range(height):
		for w in range(width):
			if pilImage.mode == 'RGB':
				r, g, b = oldImage[h * width + w]
				newImage.append((255 - r, 255 - g, 255 - b))
			elif pilImage.mode == 'L':
				newImage.append(255 - oldImage[h * width + w])

	pilImage = PILImage.new(pilImage.mode, pilImage.size)
	pilImage.putdata(newImage)
	displayImage()

def undo():
	global previousPilImage, pilImage
	pilImage = previousPilImage
	displayImage()

def lighten():
	global root, pilImage, previousPilImage
	previousPilImage = pilImage
	width, height = pilImage.size
	oldImage = pilImage.getdata()
	newImage = []
	for h in range(height):
		for w in range(width):
			pixel = oldImage[h * width + w]
			if pilImage.mode == 'L':
				val = pixel + 25
				val = val if val < 255 else 255
				newImage.append(val)
			elif pilImage.mode == 'RGB':
				val = []
				for i in pixel:
					val.append(i + 25 if i + 25 < 255 else 255)
				newImage.append(tuple(val))

	pilImage = PILImage.new(pilImage.mode, pilImage.size)
	pilImage.putdata(newImage)
	displayImage()

def darken():
	global root, pilImage, previousPilImage
	previousPilImage = pilImage
	width, height = pilImage.size
	oldImage = pilImage.getdata()
	newImage = []
	for h in range(height):
		for w in range(width):
			pixel = oldImage[h * width + w]
			if pilImage.mode == 'L':
				val = pixel - 25
				val = val if val > 0 else 0
				newImage.append(val)
			elif pilImage.mode == 'RGB':
				val = []
				for i in pixel:
					val.append(i - 25 if i - 25 > 0 else 0)
				newImage.append(tuple(val))

	pilImage = PILImage.new(pilImage.mode, pilImage.size)
	pilImage.putdata(newImage)
	displayImage()	

def sharpenBW(oldImage, width, height, sharpeningMatrix):
	newImage = []
	for h in range(height):
		for w in range(width):
			pixel = 0
			if h > 0 and w > 0:
				pixel += oldImage[(h - 1) * width + (w - 1)] * sharpeningMatrix[0][0]
			if h > 0:
				pixel += oldImage[(h - 1) * width + w] * sharpeningMatrix[0][1]
			if h > 0 and w < (width - 1):
				pixel += oldImage[(h - 1) * width + (w + 1)] * sharpeningMatrix[0][2]
			if w > 0:
				pixel += oldImage[h * width + (w - 1)] * sharpeningMatrix[1][0] 
			pixel += oldImage[h * width + w] * sharpeningMatrix[1][1]
			if w < (width - 1):
				pixel += oldImage[h * width + (w + 1)] * sharpeningMatrix[1][2]

			if h < (height - 1) and w > 0:
				pixel += oldImage[(h + 1) * width + (w - 1)] * sharpeningMatrix[2][0]
			if h < (height - 1):
				pixel += oldImage[(h + 1) * width + w] * sharpeningMatrix[2][1]
			if h < (height - 1) and w < (w - 1):
				pixel += oldImage[(h + 1) * width + (w + 1)] * sharpeningMatrix[2][2]
			pixel = int(pixel)
			if pixel < 0:
				pixel = 0
			if pixel > 255:
				pixel = 255
			newImage.append(pixel)

	return newImage

def sharpenRGB(oldImage, width, height, sharpeningMatrix):
	newImage = []
	for h in range(height):
		for w in range(width):
			pixel = [0, 0, 0]
			for i in range(3):
				if h > 0 and w > 0:
					pixel[i] += oldImage[(h - 1) * width + (w - 1)][i] * sharpeningMatrix[0][0]
				if h > 0:
					pixel[i] += oldImage[(h - 1) * width + w][i] * sharpeningMatrix[0][1]
				if h > 0 and w < (width - 1):
					pixel[i] += oldImage[(h - 1) * width + (w + 1)][i] * sharpeningMatrix[0][2]
				if w > 0:
					pixel[i] += oldImage[h * width + (w - 1)][i] * sharpeningMatrix[1][0] 
				pixel[i] += oldImage[h * width + w][i] * sharpeningMatrix[1][1]
				if w < (width - 1):
					pixel[i] += oldImage[h * width + (w + 1)][i] * sharpeningMatrix[1][2]

				if h < (height - 1) and w > 0:
					pixel[i] += oldImage[(h + 1) * width + (w - 1)][i] * sharpeningMatrix[2][0]
				if h < (height - 1):
					pixel[i] += oldImage[(h + 1) * width + w][i] * sharpeningMatrix[2][1]
				if h < (height - 1) and w < (w - 1):
					pixel[i] += oldImage[(h + 1) * width + (w + 1)][i] * sharpeningMatrix[2][2]
				pixel[i] = int(pixel[i])
				if pixel[i] < 0:
					pixel[i] = 0
				if pixel[i] > 255:
					pixel[i] = 255
			newImage.append(tuple(pixel))

	return newImage

def sharpen():
	global pilImage, previousPilImage
	width, height = pilImage.size
	if width < 2 or height < 2:
		return
	previousPilImage = pilImage

	oldImage = pilImage.getdata()
	sharpeningMatrix = [[0, -1, 0], [-1, 5, -1], [0, -1, 0]]
	
	newImage = []
	if pilImage.mode == 'L':
		newImage = sharpenBW(oldImage, width, height, sharpeningMatrix)
	elif pilImage.mode == 'RGB':
		newImage = sharpenRGB(oldImage, width, height, sharpeningMatrix)

	# chain.from_iterable(newImage)
	pilImage = PILImage.new(pilImage.mode, pilImage.size)
	pilImage.putdata(newImage)
	displayImage()

def blur():
	global pilImage, previousPilImage
	width, height = pilImage.size
	if width < 2 or height < 2:
		return
	previousPilImage = pilImage

	oldImage = pilImage.getdata()
	blurMatrix = [[0.0625, 0.125, 0.0625], [0.125, 0.25, 0.125], [0.0625, 0.125, 0.0625]]
	
	newImage = []
	if pilImage.mode == 'L':
		newImage = sharpenBW(oldImage, width, height, blurMatrix)
	elif pilImage.mode == 'RGB':
		newImage = sharpenRGB(oldImage, width, height, blurMatrix)

	pilImage = PILImage.new(pilImage.mode, pilImage.size)
	pilImage.putdata(newImage)
	displayImage()	

def rotateImg(direction):
	global pilImage, previousPilImage
	previousPilImage = pilImage

	width, height = pilImage.size
	oldImage = pilImage.getdata()
	newImage = []
	if direction == 'left':
		for w in range(width - 1, -1, -1):
			for h in range(height):
				newImage.append(oldImage[h * width + w])
	elif direction == 'right':
		for w in range(width):
			for h in range(height -1, -1, -1):
				newImage.append(oldImage[h * width + w])

	pilImage = PILImage.new(pilImage.mode, (height, width))
	pilImage.putdata(newImage)
	displayImage()

def rotateLeft():
	rotateImg('left')

def rotateRight():
	rotateImg('right')	

def zoomIn():
	global pilImage, previousPilImage
	previousPilImage = pilImage
	times = 2

	width, height = pilImage.size
	oldImage = pilImage.getdata()
	newImage = []
	for h in range(height):
		line = []
		for w in range(width):
			pixel = oldImage[h * width + w]
			for i in range(times):
				line.append(pixel)
		for i in range(times):
			for j in line:
				newImage.append(j)

	pilImage = PILImage.new(pilImage.mode, (width * 2, height * 2))
	pilImage.putdata(newImage)
	displayImage()

root = Tk()
root.title('Simple image editor')
frame = Frame(root)
menuBar = Menu(root)

scrollBarVer = Scrollbar(root, orient=VERTICAL)
scrollBarVer.pack(side=RIGHT, fill=Y)
scrollBarHor = Scrollbar(root, orient=HORIZONTAL)
scrollBarHor.pack(side=BOTTOM, fill=X)

fileMenu = Menu(menuBar, tearoff=0)
fileMenu.add_command(label='Open', command=openImage)
fileMenu.add_command(label='Save', command=saveImage)
fileMenu.add_command(label='Save as', command=saveAs)
fileMenu.add_separator()
fileMenu.add_command(label='Exit', command=root.quit)

geometryMenu = Menu(menuBar, tearoff=0)
geometryMenu.add_command(label='Flip horizontally', command=flipHor)
geometryMenu.add_command(label='Flip vertically', command=flipVer)
geometryMenu.add_command(label='Rotate left', command=rotateLeft)
geometryMenu.add_command(label='Rotate right', command=rotateRight)

editMenu = Menu(menuBar, tearoff=0)
editMenu.add_command(label='Desaturate', command=desaturate)
editMenu.add_command(label='Invert colors', command=invertColors)
editMenu.add_command(label='Lighten', command=lighten)
editMenu.add_command(label='Darken', command=darken)
editMenu.add_command(label='Sharpen', command=sharpen)
editMenu.add_command(label='Blur', command=blur)

menuBar.add_cascade(label='File', menu=fileMenu)
menuBar.add_cascade(label='Geometry', menu=geometryMenu)
menuBar.add_cascade(label='Edit', menu=editMenu)
menuBar.add_command(label='Undo', command=undo)
menuBar.add_command(label='Zoom in', command=zoomIn)

canvas = Canvas(root)
canvas.config(yscrollcommand=scrollBarVer.set, xscrollcommand=scrollBarHor.set)
canvas.pack()

scrollBarVer.config(command=canvas.yview)
scrollBarHor.config(command=canvas.xview)

root.config(menu=menuBar)
root.mainloop()
